<?php

namespace IGF\VisitasBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reclusovisitas
 *
 * @ORM\Table(name="reclusovisitas",uniqueConstraints={@ORM\UniqueConstraint(name="idreclusovisitas_pk", columns={"idreclusovisitas"})},
  indexes={@ORM\Index(name="idcarcel_fk", columns={"idcarcel"}),@ORM\Index(name="reclusovisitas_fk", columns={"idrecluso"})})
 * @ORM\Entity
 */
class Reclusovisitas
{

  /**
 * @var \Doctrine\Common\Collections\Collection
 *
 * @ORM\ManyToMany(targetEntity="\IGF\CoreBundle\Entity\Personas", inversedBy="idvisitalistas")
 * @ORM\JoinTable(name="lista_visitas",
 *   joinColumns={
 *     @ORM\JoinColumn(name="persona_id", referencedColumnName="idreclusovisitas")
 *   },
 *   inverseJoinColumns={
 *     @ORM\JoinColumn(name="visita_id", referencedColumnName="idpersona")
 *   }
 * )
 */
private $listapersonas;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="fecha", type="date", nullable=true)
    */
    private $fecha;

    /**
     * @var integer
     *
     * @ORM\Column(name="idreclusovisitas", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="reclusovisitas_idreclusovisitas_seq", allocationSize=1, initialValue=1)
     */
    private $idreclusovisitas;

    /**
     * @var \Carcels
     *
     * @ORM\ManyToOne(targetEntity="\IGF\ModeloBundle\Entity\Carcels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcarcel", referencedColumnName="idcarcel")
     * })
     */
    private $idcarcel;

    /**
     * @var \Reclusos
     *
     * @ORM\ManyToOne(targetEntity="\IGF\ModeloBundle\Entity\Reclusos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idrecluso", referencedColumnName="idrecluso")
     * })
     */
    private $idrecluso;



    /**
     * Set fechaPedido
     *
     * @param \DateTime $fecha
     * @return InvEntrada
     */
    public function setFecha($fechaPedido)
    {
        $this->fechaPedido = $fechaPedido;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Get idreclusovisitas
     *
     * @return integer
     */
    public function getIdreclusovisitas()
    {
        return $this->idreclusovisitas;
    }

    /**
     * Set idcarcel
     *
     * @param \IGF\ModeloBundle\Entity\Carcels $idcarcel
     *
     * @return Reclusovisitas
     */
    public function setIdcarcel(\IGF\ModeloBundle\Entity\Carcels $idcarcel = null)
    {
        $this->idcarcel = $idcarcel;

        return $this;
    }

    /**
     * Get idcarcel
     *
     * @return \IGF\ModeloBundle\Entity\Carcels
     */
    public function getIdcarcel()
    {
        return $this->idcarcel;
    }

    /**
     * Set idrecluso
     *
     * @param \IGF\ModeloBundle\Entity\Reclusos $idrecluso
     *
     * @return Reclusovisitas
     */
    public function setIdrecluso(\IGF\ModeloBundle\Entity\Reclusos $idrecluso = null)
    {
        $this->idrecluso = $idrecluso;

        return $this;
    }

    /**
     * Get idrecluso
     *
     * @return \IGF\ModeloBundle\Entity\Reclusos
     */
    public function getIdrecluso()
    {
        return $this->idrecluso;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->listapersonas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add listapersona
     *
     * @param \IGF\CoreBundle\Entity\Personas $listapersona
     *
     * @return Reclusovisitas
     */
    public function addListapersona(\IGF\CoreBundle\Entity\Personas $listapersona)
    {
        $this->listapersonas[] = $listapersona;

        return $this;
    }

    /**
     * Remove listapersona
     *
     * @param \IGF\CoreBundle\Entity\Personas $listapersona
     */
    public function removeListapersona(\IGF\CoreBundle\Entity\Personas $listapersona)
    {
        $this->listapersonas->removeElement($listapersona);
    }

    /**
     * Get listapersonas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListapersonas()
    {
        return $this->listapersonas;
    }
}
