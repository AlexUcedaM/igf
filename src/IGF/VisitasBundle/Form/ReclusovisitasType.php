<?php

namespace IGF\VisitasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use IGF\ModeloBundle\Entity\Carcels;
use IGF\ModeloBundle\Entity\Reclusos;
use IGF\CoreBundle\Entity\Personas;

class ReclusovisitasType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('idcarcel','entity',array('class' => Carcels::class,'placeholder' => 'idcarcel','choice_label'=>'nombre',
        'attr'=> array('class' => 'form-control')))
        ->add('idrecluso','entity',array('class' => Reclusos::class,'placeholder' => 'idrecluso','choice_label'=>'numerorecluso',
        'attr'=> array('class' => 'form-control')))
        ->add('fecha', 'date',       array('required' => true,'widget' => 'single_text', 'data' => new \DateTime("now"),
            'attr'=> array('class' => 'form-control')))
        ->add('listapersonas', 'entity', array('required' => false,'class' => Personas::class, 'choice_label'=>'nombres', 'multiple'=>true,'label_attr' => array('class' => 'checkbox-inline')));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IGF\VisitasBundle\Entity\Reclusovisitas'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'igf_visitasbundle_reclusovisitas';
    }


}
