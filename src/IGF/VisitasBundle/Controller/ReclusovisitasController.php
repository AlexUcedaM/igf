<?php

namespace IGF\VisitasBundle\Controller;

use IGF\VisitasBundle\Entity\Reclusovisitas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Reclusovisita controller.
 *
 * @Route("reclusovisitas")
 */
class ReclusovisitasController extends Controller
{
    /**
     * Lists all reclusovisita entities.
     *
     * @Route("/", name="reclusovisitas_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reclusovisitas = $em->getRepository('IGFVisitasBundle:Reclusovisitas')->findAll();

        return $this->render('IGFVisitasBundle:reclusovisitas:index.html.twig', array(
            'reclusovisitas' => $reclusovisitas,
        ));
    }

    /**
     * Creates a new reclusovisita entity.
     *
     * @Route("/new", name="reclusovisitas_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $reclusovisita = new Reclusovisitas();
        $form = $this->createForm('IGF\VisitasBundle\Form\ReclusovisitasType', $reclusovisita);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reclusovisita);
            $em->flush();

            return $this->redirectToRoute('reclusovisitas_show', array('idreclusovisitas' => $reclusovisita->getIdreclusovisitas()));
        }

        return $this->render('IGFVisitasBundle:reclusovisitas:new.html.twig', array(
            'reclusovisita' => $reclusovisita,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a reclusovisita entity.
     *
     * @Route("/{idreclusovisitas}", name="reclusovisitas_show")
     * @Method("GET")
     */
    public function showAction(Reclusovisitas $reclusovisita)
    {
        $deleteForm = $this->createDeleteForm($reclusovisita);

        return $this->render('IGFVisitasBundle:reclusovisitas:show.html.twig', array(
            'reclusovisita' => $reclusovisita,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing reclusovisita entity.
     *
     * @Route("/{idreclusovisitas}/edit", name="reclusovisitas_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Reclusovisitas $reclusovisita)
    {
        $deleteForm = $this->createDeleteForm($reclusovisita);
        $editForm = $this->createForm('IGF\VisitasBundle\Form\ReclusovisitasType', $reclusovisita);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reclusovisitas_edit', array('idreclusovisitas' => $reclusovisita->getIdreclusovisitas()));
        }

        return $this->render('IGFVisitasBundle:reclusovisitas:edit.html.twig', array(
            'reclusovisita' => $reclusovisita,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a reclusovisita entity.
     *
     * @Route("/{idreclusovisitas}", name="reclusovisitas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Reclusovisitas $reclusovisita)
    {
        $form = $this->createDeleteForm($reclusovisita);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reclusovisita);
            $em->flush();
        }

        return $this->redirectToRoute('reclusovisitas_index');
    }

    /**
     * Creates a form to delete a reclusovisita entity.
     *
     * @param Reclusovisitas $reclusovisita The reclusovisita entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reclusovisitas $reclusovisita)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reclusovisitas_delete', array('idreclusovisitas' => $reclusovisita->getIdreclusovisitas())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
