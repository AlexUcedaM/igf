<?php

namespace IGF\VisitasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IGFVisitasBundle:Default:index.html.twig');
    }
}
