<?php

namespace IGF\PlantillaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IGFPlantillaBundle:Default:index.html.twig');
    }
}
