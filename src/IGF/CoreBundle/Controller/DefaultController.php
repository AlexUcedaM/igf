<?php

namespace IGF\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IGFCoreBundle:Default:index.html.twig');
    }
}
