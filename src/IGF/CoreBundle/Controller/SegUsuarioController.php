<?php

namespace IGF\CoreBundle\Controller;

use IGF\CoreBundle\Entity\SegUsuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Segusuario controller.
 *
 * @Route("usuarios")
 */
class SegUsuarioController extends Controller
{
    /**
     * Lists all segUsuario entities.
     *
     * @Route("/", name="usuarios_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $segUsuarios = $em->getRepository('IGFCoreBundle:SegUsuario')->findAll();

        return $this->render('IGFCoreBundle:segusuario:index.html.twig', array(
            'segUsuarios' => $segUsuarios,
        ));
    }

    /**
     * Creates a new segUsuario entity.
     *
     * @Route("/new", name="usuarios_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $segUsuario = new Segusuario();
        $form = $this->createForm('IGF\CoreBundle\Form\SegUsuarioType', $segUsuario);
        $form->remove('parentesco');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($segUsuario);
            $em->flush();

            return $this->redirectToRoute('usuarios_show', array('id' => $segUsuario->getId()));
        }

        return $this->render('IGFCoreBundle:segusuario:new.html.twig', array(
            'segUsuario' => $segUsuario,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a segUsuario entity.
     *
     * @Route("/{id}", name="usuarios_show")
     * @Method("GET")
     */
    public function showAction(SegUsuario $segUsuario)
    {
        $deleteForm = $this->createDeleteForm($segUsuario);

        return $this->render('IGFCoreBundle:segusuario:show.html.twig', array(
            'segUsuario' => $segUsuario,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing segUsuario entity.
     *
     * @Route("/{id}/edit", name="usuarios_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SegUsuario $segUsuario)
    {
        $deleteForm = $this->createDeleteForm($segUsuario);
        $editForm = $this->createForm('IGF\CoreBundle\Form\SegUsuarioType', $segUsuario);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('usuarios_edit', array('id' => $segUsuario->getId()));
        }

        return $this->render('IGFCoreBundle:segusuario:edit.html.twig', array(
            'segUsuario' => $segUsuario,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a segUsuario entity.
     *
     * @Route("/{id}", name="usuarios_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SegUsuario $segUsuario)
    {
        $form = $this->createDeleteForm($segUsuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($segUsuario);
            $em->flush();
        }

        return $this->redirectToRoute('usuarios_index');
    }

    /**
     * Creates a form to delete a segUsuario entity.
     *
     * @param SegUsuario $segUsuario The segUsuario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SegUsuario $segUsuario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuarios_delete', array('id' => $segUsuario->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
