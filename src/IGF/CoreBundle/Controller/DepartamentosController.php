<?php

namespace IGF\CoreBundle\Controller;

use IGF\CoreBundle\Entity\Departamentos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Departamento controller.
 *
 * @Route("departamentos")
 */
class DepartamentosController extends Controller
{
    /**
     * Lists all departamento entities.
     *
     * @Route("/", name="departamentos_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $departamentos = $em->getRepository('IGFCoreBundle:Departamentos')->findAll();

        return $this->render('IGFCoreBundle:departamentos:index.html.twig', array(
            'departamentos' => $departamentos,
        ));
    }

    /**
     * Creates a new departamento entity.
     *
     * @Route("/new", name="departamentos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $departamento = new Departamentos();
        $form = $this->createForm('IGF\CoreBundle\Form\DepartamentosType', $departamento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($departamento);
            $em->flush();

            return $this->redirectToRoute('departamentos_show', array('iddepartamento' => $departamento->getIddepartamento()));
        }

        return $this->render('IGFCoreBundle:departamentos:new.html.twig', array(
            'departamento' => $departamento,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a departamento entity.
     *
     * @Route("/{iddepartamento}", name="departamentos_show")
     * @Method("GET")
     */
    public function showAction(Departamentos $departamento)
    {
        $deleteForm = $this->createDeleteForm($departamento);

        return $this->render('IGFCoreBundle:departamentos:show.html.twig', array(
            'departamento' => $departamento,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing departamento entity.
     *
     * @Route("/{iddepartamento}/edit", name="departamentos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Departamentos $departamento)
    {
        $deleteForm = $this->createDeleteForm($departamento);
        $editForm = $this->createForm('IGF\CoreBundle\Form\DepartamentosType', $departamento);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('departamentos_edit', array('iddepartamento' => $departamento->getIddepartamento()));
        }

        return $this->render('IGFCoreBundle:departamentos:edit.html.twig', array(
            'departamento' => $departamento,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a departamento entity.
     *
     * @Route("/{iddepartamento}", name="departamentos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Departamentos $departamento)
    {
        $form = $this->createDeleteForm($departamento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($departamento);
            $em->flush();
        }

        return $this->redirectToRoute('departamentos_index');
    }

    /**
     * Creates a form to delete a departamento entity.
     *
     * @param Departamentos $departamento The departamento entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Departamentos $departamento)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('departamentos_delete', array('iddepartamento' => $departamento->getIddepartamento())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
