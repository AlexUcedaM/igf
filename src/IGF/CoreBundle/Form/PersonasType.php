<?php

namespace IGF\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonasType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
        ->add('nombres','text',array('pattern'=>'[A-Za-z]','required'=>false,'attr'=>array('placeholder'=>'Ingrese el nombre','class'=>'form-control')))
        ->add('apellidos','text',array('pattern'=>'[A-Za-z]','required'=>false,'attr'=>array('placeholder'=>'Ingrese el nombre','class'=>'form-control')))
        ->add('dui','text',array('attr'=> array('placeholder' => '########-#', 'class' => 'form-control')))
        //'pattern' => '\d{8}-\d{1}',

        ->add('nit','text',array('attr'=> array('placeholder' => '####-######-###-#', 'class' => 'form-control')))
        //'pattern' => '\d{4}-\d{6}-\d{3}-\d{1}',

        ->add('telefono' ,'text',array('required' => false,'attr'=> array('placeholder' => '####-####', 'class' => 'form-control')))
        //'pattern' => '\d{4}-\d{4}',
        ->add('celular','text',array('required' => false,'attr'=> array('pattern' => '\d{4}-\d{4}', 'class' => 'form-control')))
        //'placeholder' => '####-####',
        ->add('parentesco','text',array('attr'=> array('placeholder' => 'Parentesco con el recluso', 'required'=>false,'class' => 'form-control')))
        //'pattern' => '[A-Za-z]',
        ->add('foto')
        ->add('foto2');

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IGF\CoreBundle\Entity\Personas'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'igf_corebundle_personas';
    }


}
