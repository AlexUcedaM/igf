<?php

namespace IGF\CoreBundle\Form;

use IGF\CoreBundle\Entity\Municipios;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DetalledireccionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('direccion','text',array('required'=>false,'attr'=>array('placeholder'=>'Ingrese la dirección','class'=>'form-control')))
        ->add('idmunicipio','entity',array('class' => Municipios::class,
          'placeholder' => 'municipio',
          'required' => true,
          'choice_label'=>'nombremunicipio',
          'attr'=> array('class' => 'form-control')));

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IGF\CoreBundle\Entity\Detalledireccion'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'igf_corebundle_detalledireccion';
    }


}
