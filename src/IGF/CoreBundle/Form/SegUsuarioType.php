<?php

namespace IGF\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use IGF\CoreBundle\Form\PersonasType;
use IGF\CoreBundle\Entity\SegRol;
use IGF\ModeloBundle\Entity\Carcels;
class SegUsuarioType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('usuario','text',array('pattern'=>'[A-Za-z]','required'=>false,'attr'=>array('placeholder'=>'Ingrese el nombre','class'=>'form-control')))
        ->add('password','password', array('label'=>'Contraseña', 'attr' => array('type'=> 'password' ,'placeholder' =>  'Contraseña', 'class' => 'form-control')))
        ->add('nombre','text',array('pattern'=>'[A-Za-z]','required'=>false,'attr'=>array('placeholder'=>'Ingrese el nombre','class'=>'form-control')))
        ->add('idSegRol', 'entity',
        array('required' => false,'class' => SegRol::class, 'choice_label'=>'nombre','label'=>'Seleccione los roles', 'multiple'=>true,'label_attr' =>
          array('class' => 'checkbox-inline', 'class'=>'form-control')))
        ->add('idcarcel', 'entity',
          array('required' => false,'class' => Carcels::class, 'choice_label'=>'nombre','label'=>'Seleccione los roles', 'multiple'=>true,'label_attr' =>
            array('class' => 'checkbox-inline', 'class'=>'form-control')));

        $builder->add('idpersona', new PersonasType(), array('label'=>'DATOS PERSONALES'));



    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IGF\CoreBundle\Entity\SegUsuario'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'igf_corebundle_segusuario';
    }


}
