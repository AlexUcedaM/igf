<?php

namespace IGF\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SegUsuario
 *
 * @ORM\Table(name="seg_usuario", uniqueConstraints={@ORM\UniqueConstraint(name="usuarios_pk", columns={"id"})}, indexes={@ORM\Index(name="fki_idpersonas", columns={"idpersona"})})
 * @ORM\Entity
 */
class SegUsuario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="seg_usuario_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=40, nullable=false)
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=15, nullable=false)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="date", nullable=true)
     */
    private $fechaModificacion;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=25, nullable=true)
     */
    private $nombre;

    /**
     * @var \Personas
     *
     * @ORM\ManyToOne(targetEntity="Personas", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpersona", referencedColumnName="idpersona")
     * })
     */
    private $idpersona;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="\IGF\ModeloBundle\Entity\Carcels", mappedBy="idusuario")
     */
    private $idcarcel;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SegRol", inversedBy="segUsuarioid")
     * @ORM\JoinTable(name="seg_usuarios_roles",
     *   joinColumns={
     *     @ORM\JoinColumn(name="seg_usuarioid", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_seg_rol", referencedColumnName="id")
     *   }
     * )
     */
    private $idSegRol;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idcarcel = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idSegRol = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     *
     * @return SegUsuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return SegUsuario
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return SegUsuario
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return SegUsuario
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return SegUsuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set idpersona
     *
     * @param \IGF\CoreBundle\Entity\Personas $idpersona
     *
     * @return SegUsuario
     */
    public function setIdpersona(\IGF\CoreBundle\Entity\Personas $idpersona = null)
    {
        $this->idpersona = $idpersona;

        return $this;
    }

    /**
     * Get idpersona
     *
     * @return \IGF\CoreBundle\Entity\Personas
     */
    public function getIdpersona()
    {
        return $this->idpersona;
    }

    /**
     * Add idcarcel
     *
     * @param \IGF\ModeloBundle\Entity\Carcels $idcarcel
     *
     * @return SegUsuario
     */
    public function addIdcarcel(\IGF\ModeloBundle\Entity\Carcels $idcarcel)
    {
        $this->idcarcel[] = $idcarcel;

        return $this;
    }

    /**
     * Remove idcarcel
     *
     * @param \IGF\ModeloBundle\Entity\Carcels $idcarcel
     */
    public function removeIdcarcel(\IGF\ModeloBundle\Entity\Carcels $idcarcel)
    {
        $this->idcarcel->removeElement($idcarcel);
    }

    /**
     * Get idcarcel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdcarcel()
    {
        return $this->idcarcel;
    }

    /**
     * Add idSegRol
     *
     * @param \IGF\CoreBundle\Entity\SegRol $idSegRol
     *
     * @return SegUsuario
     */
    public function addIdSegRol(\IGF\CoreBundle\Entity\SegRol $idSegRol)
    {
        $this->idSegRol[] = $idSegRol;

        return $this;
    }

    /**
     * Remove idSegRol
     *
     * @param \IGF\CoreBundle\Entity\SegRol $idSegRol
     */
    public function removeIdSegRol(\IGF\CoreBundle\Entity\SegRol $idSegRol)
    {
        $this->idSegRol->removeElement($idSegRol);
    }

    /**
     * Get idSegRol
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdSegRol()
    {
        return $this->idSegRol;
    }
}
