<?php

namespace IGF\CoreBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Personas
 *
 * @ORM\Table(name="personas", uniqueConstraints={@ORM\UniqueConstraint(name="personas_pk", columns={"idpersona"})}, indexes={@ORM\Index(name="relationship_27_fk", columns={"iddetalledireccion"})})
 * @ORM\Entity
 */
class Personas
{


  /**
   * @var \Doctrine\Common\Collections\Collection
   *
   * @ORM\ManyToMany(targetEntity="\IGF\VisitasBundle\Entity\Reclusovisitas", mappedBy="listapersonas")
   */
  private $idvisitalistas;


    /**
     * @var integer
     *
     * @ORM\Column(name="idpersona", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="personas_idpersona_seq", allocationSize=1, initialValue=1)
     */
    private $idpersona;

    /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", length=40, nullable=false)
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=40, nullable=false)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="dui", type="string", nullable=false)
     */
    private $dui;

    /**
     * @var string
     *
     * @ORM\Column(name="nit", type="string", nullable=false)
     */
    private $nit;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=9, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="celular", type="string", length=10, nullable=true)
     */
    private $celular;

    /**
     * @var string
     *
     * @ORM\Column(name="detalledireccion", type="string", length=40, nullable=true)
     */
    private $detalledireccion;

    /**
     * @var string
     *
     * @ORM\Column(name="parentesco", type="string", length=20, nullable=true)
     */
    private $parentesco;

    /**
     * @var string
     *
     * @ORM\Column(name="foto", type="string", length=20, nullable=true)
     */
    private $foto;

    /**
     * @var string
     *
     * @ORM\Column(name="foto2", type="string", length=20, nullable=true)
     */
    private $foto2;

    /**
     * Get
     *
     * @return string
     */
    public function getNombrecompleto()
    {
        $completo= "".$this->getNombres().",".$this->getApellidos();
        return $completo;
    }

    /**
     * @var \Detalledireccion
     *
     * @ORM\ManyToOne(targetEntity="\IGF\CoreBundle\Entity\Detalledireccion", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddetalledireccion", referencedColumnName="iddetalledireccion")
     * })
     */
    private $iddetalledireccion;



    /**
     * Get idpersona
     *
     * @return integer
     */
    public function getIdpersona()
    {
        return $this->idpersona;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     *
     * @return Personas
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return Personas
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set dui
     *
     * @param string $dui
     *
     * @return Personas
     */
    public function setDui($dui)
    {
        $this->dui = $dui;

        return $this;
    }

    /**
     * Get dui
     *
     * @return string
     */
    public function getDui()
    {
        return $this->dui;
    }

    /**
     * Set nit
     *
     * @param string $nit
     *
     * @return Personas
     */
    public function setNit($nit)
    {
        $this->nit = $nit;

        return $this;
    }

    /**
     * Get nit
     *
     * @return string
     */
    public function getNit()
    {
        return $this->nit;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Personas
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     *
     * @return Personas
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set detalledireccion
     *
     * @param string $detalledireccion
     *
     * @return Personas
     */
    public function setDetalledireccion($detalledireccion)
    {
        $this->detalledireccion = $detalledireccion;

        return $this;
    }

    /**
     * Get detalledireccion
     *
     * @return string
     */
    public function getDetalledireccion()
    {
        return $this->detalledireccion;
    }

    /**
     * Set parentesco
     *
     * @param string $parentesco
     *
     * @return Personas
     */
    public function setParentesco($parentesco)
    {
        $this->parentesco = $parentesco;

        return $this;
    }

    /**
     * Get parentesco
     *
     * @return string
     */
    public function getParentesco()
    {
        return $this->parentesco;
    }

    /**
     * Set foto
     *
     * @param string $foto
     *
     * @return Personas
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set foto2
     *
     * @param string $foto2
     *
     * @return Personas
     */
    public function setFoto2($foto2)
    {
        $this->foto2 = $foto2;

        return $this;
    }

    /**
     * Get foto2
     *
     * @return string
     */
    public function getFoto2()
    {
        return $this->foto2;
    }

    /**
     * Set iddetalledireccion
     *
     * @param \IGF\CoreBundle\Entity\Detalledireccion $iddetalledireccion
     *
     * @return Personas
     */
    public function setIddetalledireccion(\IGF\CoreBundle\Entity\Detalledireccion $iddetalledireccion = null)
    {
        $this->iddetalledireccion = $iddetalledireccion;

        return $this;
    }

    /**
     * Get iddetalledireccion
     *
     * @return \IGF\CoreBundle\Entity\Detalledireccion
     */
    public function getIddetalledireccion()
    {
        return $this->iddetalledireccion;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idvisitalistas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get idlistavisitas
     *
     * @return integer
     */
    public function getIdlistavisitas()
    {
        return $this->idlistavisitas;
    }

    /**
     * Add idvisitalista
     *
     * @param \IGF\VisitasBundle\Entity\Reclusovisitas $idvisitalista
     *
     * @return Personas
     */
    public function addIdvisitalista(\IGF\VisitasBundle\Entity\Reclusovisitas $idvisitalista)
    {
        $this->idvisitalistas[] = $idvisitalista;

        return $this;
    }

    /**
     * Remove idvisitalista
     *
     * @param \IGF\VisitasBundle\Entity\Reclusovisitas $idvisitalista
     */
    public function removeIdvisitalista(\IGF\VisitasBundle\Entity\Reclusovisitas $idvisitalista)
    {
        $this->idvisitalistas->removeElement($idvisitalista);
    }

    /**
     * Get idvisitalistas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdvisitalistas()
    {
        return $this->idvisitalistas;
    }
}
