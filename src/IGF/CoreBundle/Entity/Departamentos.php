<?php

namespace IGF\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departamentos
 *
 * @ORM\Table(name="departamentos", uniqueConstraints={@ORM\UniqueConstraint(name="departamentos_pk", columns={"iddepartamento"})})
 * @ORM\Entity
 */
class Departamentos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="iddepartamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="departamentos_iddepartamento_seq", allocationSize=1, initialValue=1)
     */
    private $iddepartamento;

    /**
     * @var string
     *
     * @ORM\Column(name="nombredepartamento", type="string", length=20, nullable=false)
     */
    private $nombredepartamento;



    /**
     * Get iddepartamento
     *
     * @return integer
     */
    public function getIddepartamento()
    {
        return $this->iddepartamento;
    }

    /**
     * Set nombredepartamento
     *
     * @param string $nombredepartamento
     *
     * @return Departamentos
     */
    public function setNombredepartamento($nombredepartamento)
    {
        $this->nombredepartamento = $nombredepartamento;

        return $this;
    }

    /**
     * Get nombredepartamento
     *
     * @return string
     */
    public function getNombredepartamento()
    {
        return $this->nombredepartamento;
    }
    
    /**
     * Get nombredepartamento
     *
     * @return string
     */
    public function getDepartamento()
    {
        return $this->nombredepartamento;
    }
}
