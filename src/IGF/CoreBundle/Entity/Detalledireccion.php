<?php

namespace IGF\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Detalledireccion
 *
 * @ORM\Table(name="detalledireccion", uniqueConstraints={@ORM\UniqueConstraint(name="detalledireccion_pk", columns={"iddetalledireccion"})}, indexes={@ORM\Index(name="municipiosdireccion_fk", columns={"idmunicipio"})})
 * @ORM\Entity
 */
class Detalledireccion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="iddetalledireccion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="detalledireccion_iddetalledireccion_seq", allocationSize=1, initialValue=1)
     */
    private $iddetalledireccion;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="text", nullable=false)
     */
    private $direccion;

    /**
     * @var \Municipios
     *
     * @ORM\ManyToOne(targetEntity="Municipios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idmunicipio", referencedColumnName="idmunicipio")
     * })
     */
    private $idmunicipio;



    /**
     * Get iddetalledireccion
     *
     * @return integer
     */
    public function getIddetalledireccion()
    {
        return $this->iddetalledireccion;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Detalledireccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set idmunicipio
     *
     * @param \IGF\CoreBundle\Entity\Municipios $idmunicipio
     *
     * @return Detalledireccion
     */
    public function setIdmunicipio(\IGF\CoreBundle\Entity\Municipios $idmunicipio = null)
    {
        $this->idmunicipio = $idmunicipio;

        return $this;
    }

    /**
     * Get idmunicipio
     *
     * @return \IGF\CoreBundle\Entity\Municipios
     */
    public function getIdmunicipio()
    {
        return $this->idmunicipio;
    }
}
