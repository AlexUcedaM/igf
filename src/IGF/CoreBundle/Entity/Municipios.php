<?php

namespace IGF\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Municipios
 *
 * @ORM\Table(name="municipios", uniqueConstraints={@ORM\UniqueConstraint(name="municipios_pk", columns={"idmunicipio"})}, indexes={@ORM\Index(name="municipiodepartamento_fk", columns={"iddepartamento"})})
 * @ORM\Entity
 */
class Municipios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idmunicipio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="municipios_idmunicipio_seq", allocationSize=1, initialValue=1)
     */
    private $idmunicipio;

    /**
     * @var string
     *
     * @ORM\Column(name="nombremunicipio", type="string", length=20, nullable=false)
     */
    private $nombremunicipio;

    /**
     * @var \Departamentos
     *
     * @ORM\ManyToOne(targetEntity="Departamentos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddepartamento", referencedColumnName="iddepartamento")
     * })
     */
    private $iddepartamento;



    /**
     * Get idmunicipio
     *
     * @return integer
     */
    public function getIdmunicipio()
    {
        return $this->idmunicipio;
    }

    /**
     * Set nombremunicipio
     *
     * @param string $nombremunicipio
     *
     * @return Municipios
     */
    public function setNombremunicipio($nombremunicipio)
    {
        $this->nombremunicipio = $nombremunicipio;

        return $this;
    }

    /**
     * Get nombremunicipio
     *
     * @return string
     */
    public function getNombremunicipio()
    {
        return $this->nombremunicipio;
    }

    /**
     * Set iddepartamento
     *
     * @param \IGF\CoreBundle\Entity\Departamentos $iddepartamento
     *
     * @return Municipios
     */
    public function setIddepartamento(\IGF\CoreBundle\Entity\Departamentos $iddepartamento = null)
    {
        $this->iddepartamento = $iddepartamento;

        return $this;
    }

    /**
     * Get iddepartamento
     *
     * @return \IGF\CoreBundle\Entity\Departamentos
     */
    public function getIddepartamento()
    {
        return $this->iddepartamento;
    }
}
