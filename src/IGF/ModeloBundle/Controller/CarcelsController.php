<?php

namespace IGF\ModeloBundle\Controller;

use IGF\ModeloBundle\Entity\Carcels;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Carcel controller.
 *
 * @Route("carcels")
 */
class CarcelsController extends Controller
{
    /**
     * Lists all carcel entities.
     *
     * @Route("/", name="carcels_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $carcels = $em->getRepository('IGFModeloBundle:Carcels')->findAll();

        return $this->render('IGFPlantillaBundle:carcels:index.html.twig', array(
            'carcels' => $carcels,
        ));
    }

    /**
     * Creates a new carcel entity.
     *
     * @Route("/new", name="carcels_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $carcel = new Carcels();
        $form = $this->createForm('IGF\ModeloBundle\Form\CarcelsType', $carcel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($carcel);
            $em->flush();

            return $this->redirectToRoute('carcels_show', array('idcarcel' => $carcel->getIdcarcel()));
        }

        return $this->render('IGFPlantillaBundle:carcels:new.html.twig', array(
            'carcel' => $carcel,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a carcel entity.
     *
     * @Route("/{idcarcel}", name="carcels_show")
     * @Method("GET")
     */
    public function showAction(Carcels $carcel)
    {
        $deleteForm = $this->createDeleteForm($carcel);

        return $this->render('IGFPlantillaBundle:carcels:show.html.twig', array(
            'carcel' => $carcel,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing carcel entity.
     *
     * @Route("/{idcarcel}/edit", name="carcels_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Carcels $carcel)
    {
        $deleteForm = $this->createDeleteForm($carcel);
        $editForm = $this->createForm('IGF\ModeloBundle\Form\CarcelsType', $carcel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('carcels_edit', array('idcarcel' => $carcel->getIdcarcel()));
        }

        return $this->render('IGFPlantillaBundle:carcels:edit.html.twig', array(
            'carcel' => $carcel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a carcel entity.
     *
     * @Route("/{idcarcel}", name="carcels_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Carcels $carcel)
    {
        $form = $this->createDeleteForm($carcel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($carcel);
            $em->flush();
        }

        return $this->redirectToRoute('carcels_index');
    }

    /**
     * Creates a form to delete a carcel entity.
     *
     * @param Carcels $carcel The carcel entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Carcels $carcel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('carcels_delete', array('idcarcel' => $carcel->getIdcarcel())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
