<?php

namespace IGF\ModeloBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IGFModeloBundle:Default:index.html.twig');
    }
}
