<?php

namespace IGF\ModeloBundle\Controller;

use IGF\ModeloBundle\Entity\Reclusos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Recluso controller.
 *
 * @Route("reclusos")
 */
class ReclusosController extends Controller
{
    /**
     * Lists all recluso entities.
     *
     * @Route("/", name="reclusos_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reclusos = $em->getRepository('IGFModeloBundle:Reclusos')->findAll();

        return $this->render('IGFPlantillaBundle:reclusos:index.html.twig', array(
            'reclusos' => $reclusos,
        ));
    }

    /**
     * Creates a new recluso entity.
     *
     * @Route("/new", name="reclusos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $recluso = new Reclusos();
        $form = $this->createForm('IGF\ModeloBundle\Form\reclusosType', $recluso);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($recluso);
            $em->flush();

            return $this->redirectToRoute('reclusos_show', array('idrecluso' => $recluso->getIdrecluso()));
        }

        return $this->render('IGFPlantillaBundle:reclusos:new.html.twig', array(
            'recluso' => $recluso,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a recluso entity.
     *
     * @Route("/{idrecluso}", name="reclusos_show")
     * @Method("GET")
     */
    public function showAction(Reclusos $recluso)
    {
        $deleteForm = $this->createDeleteForm($recluso);

        return $this->render('IGFPlantillaBundle:reclusos:show.html.twig', array(
            'recluso' => $recluso,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing recluso entity.
     *
     * @Route("/{idrecluso}/edit", name="reclusos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Reclusos $recluso)
    {
        $deleteForm = $this->createDeleteForm($recluso);
        $editForm = $this->createForm('IGF\ModeloBundle\Form\reclusosType', $recluso);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reclusos_edit', array('idrecluso' => $recluso->getIdrecluso()));
        }

        return $this->render('IGFPlantillaBundle:reclusos:edit.html.twig', array(
            'recluso' => $recluso,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a recluso entity.
     *
     * @Route("/{idrecluso}", name="reclusos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Reclusos $recluso)
    {
        $form = $this->createDeleteForm($recluso);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($recluso);
            $em->flush();
        }

        return $this->redirectToRoute('reclusos_index');
    }

    /**
     * Creates a form to delete a recluso entity.
     *
     * @param Reclusos $recluso The recluso entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reclusos $recluso)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reclusos_delete', array('idrecluso' => $recluso->getIdrecluso())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
