<?php

namespace IGF\ModeloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use IGF\CoreBundle\Form\DetalledireccionType;
use IGF\CoreBundle\Entity\SegUsuario;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;/////////////////
class CarcelsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
->add('capacidad','integer',array('required'=>false,'attr'=>array('placeholder'=>'Ingrese la capacidad de la carcel','class'=>'form-control')))
->add('nombre','text',array('pattern'=>'[A-Za-z]','required'=>false,'attr'=>array('placeholder'=>'Ingrese el nombre de la carcel','class'=>'form-control')))
->add('cantidad_celdas','integer',array('required'=>false,'attr'=>array('placeholder'=>'Ingrese la cantidad de celdas','class'=>'form-control')))
->add('idusuarioadministrador','entity',array('class' => SegUsuario::class,
    'placeholder' => 'Seleccione el administrador',
    'required' => true,'choice_label'=>'idpersona.nombrecompleto',
    'attr'=> array('class' => 'form-control')))

//->add('iddetalledireccion','entity',array('class' => Detalledireccion::class,
//    'placeholder' => 'direccion','required' => true,
//    'choice_label'=>'iddetalledireccion',
//    'attr'=> array('class' => 'form-control')))
;

$builder->add('iddetalledireccion', new DetalledireccionType(), array('label'=>'DATOS DE DIRECCION'));

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IGF\ModeloBundle\Entity\Carcels'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'igf_modelobundle_carcels';
    }


}
