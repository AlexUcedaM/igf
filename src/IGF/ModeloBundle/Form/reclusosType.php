<?php

namespace IGF\ModeloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use IGF\ModeloBundle\Entity\Carcels;
class reclusosType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('numerorecluso','text',array('required' => false,'attr'=> array('placeholder' => 'Ingrese numero de NUP', 'class' => 'form-control')))
        //'pattern' => '[0-9]',
        ->add('iniciocondena','date',array('required' => false, 'widget' => 'single_text','attr'=> array('class' => 'form-control')))
        ->add('fincondena','date',array('required' => false, 'widget' => 'single_text','attr'=> array('class' => 'form-control')))
        ->add('delito','textarea',array('attr'=> array('placeholder' => 'Puede describir el o los delitos', 'class' => 'form-control')))
        ->add('mara','textarea',array('attr'=> array('placeholder' => 'En caso de pertencer a una mara', 'class' => 'form-control')))
        ->add('idcarcel', 'entity', array('class' => Carcels::class,'placeholder' => 'Seleccione la carcel','required' => true,'choice_label'=>'nombre','attr'=> array('class' => 'form-control')));


$builder->add('idpersona', new PersonasType(), array('label'=>'DATOS PERSONALES'));

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IGF\ModeloBundle\Entity\reclusos'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'igf_modelobundle_reclusos';
    }


}
