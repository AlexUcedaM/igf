<?php

namespace IGF\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reconsideracions
 *
 * @ORM\Table(name="reconsideracions", uniqueConstraints={@ORM\UniqueConstraint(name="reconsideracions_pk", columns={"idreconsideracion"})}, indexes={@ORM\Index(name="reconsideraciones_fk", columns={"idrecluso"})})
 * @ORM\Entity
 */
class Reconsideracions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idreconsideracion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="reconsideracions_idreconsideracion_seq", allocationSize=1, initialValue=1)
     */
    private $idreconsideracion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finreconsideracion", type="date", nullable=false)
     */
    private $finreconsideracion;

    /**
     * @var string
     *
     * @ORM\Column(name="causa", type="string", length=100, nullable=true)
     */
    private $causa;

    /**
     * @var \Reclusos
     *
     * @ORM\ManyToOne(targetEntity="Reclusos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idrecluso", referencedColumnName="idrecluso")
     * })
     */
    private $idrecluso;


}

