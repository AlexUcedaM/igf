<?php

namespace IGF\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipousuario
 *
 * @ORM\Table(name="tipousuario", uniqueConstraints={@ORM\UniqueConstraint(name="tipousuario_pk", columns={"idtipousuario"})})
 * @ORM\Entity
 */
class Tipousuario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idtipousuario", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="tipousuario_idtipousuario_seq", allocationSize=1, initialValue=1)
     */
    private $idtipousuario;

    /**
     * @var string
     *
     * @ORM\Column(name="nombretipousuario", type="string", length=15, nullable=false)
     */
    private $nombretipousuario;


}

