<?php

namespace IGF\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Traslados
 *
 * @ORM\Table(name="traslados", uniqueConstraints={@ORM\UniqueConstraint(name="traslados_pk", columns={"idtraslado"})}, indexes={@ORM\Index(name="iniciot_fk", columns={"idiniciocarcel"}), @ORM\Index(name="fint_fk", columns={"idfincarcel"})})
 * @ORM\Entity
 */
class Traslados
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idtraslado", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="traslados_idtraslado_seq", allocationSize=1, initialValue=1)
     */
    private $idtraslado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechatraslado", type="date", nullable=true)
     */
    private $fechatraslado;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer", nullable=true)
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="placaauto", type="text", nullable=true)
     */
    private $placaauto;

    /**
     * @var \Carcels
     *
     * @ORM\ManyToOne(targetEntity="\IGF\ModeloBundle\Entity\Carcels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idfincarcel", referencedColumnName="idcarcel")
     * })
     */
    private $idfincarcel;

    /**
     * @var \Carcels
     *
     * @ORM\ManyToOne(targetEntity="\IGF\ModeloBundle\Entity\Carcels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idiniciocarcel", referencedColumnName="idcarcel")
     * })
     */
    private $idiniciocarcel;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Reclusos", inversedBy="idtraslado")
     * @ORM\JoinTable(name="listadoreclusotraslado",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idtraslado", referencedColumnName="idtraslado")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idrecluso", referencedColumnName="idrecluso")
     *   }
     * )
     */
    private $idrecluso;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idrecluso = new \Doctrine\Common\Collections\ArrayCollection();
    }

}

