<?php

namespace IGF\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Programas
 *
 * @ORM\Table(name="programas", uniqueConstraints={@ORM\UniqueConstraint(name="programas_pk", columns={"idprograma"})}, indexes={@ORM\Index(name="relationship_34_fk", columns={"idtipoprograma"})})
 * @ORM\Entity
 */
class Programas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idprograma", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="programas_idprograma_seq", allocationSize=1, initialValue=1)
     */
    private $idprograma;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreprograma", type="string", length=40, nullable=false)
     */
    private $nombreprograma;

    /**
     * @var string
     *
     * @ORM\Column(name="descricionprograma", type="string", length=100, nullable=true)
     */
    private $descricionprograma;

    /**
     * @var \Tipoprograma
     *
     * @ORM\ManyToOne(targetEntity="Tipoprograma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idtipoprograma", referencedColumnName="idtipoprograma")
     * })
     */
    private $idtipoprograma;


}

