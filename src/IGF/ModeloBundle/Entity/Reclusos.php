<?php

namespace IGF\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reclusos
 *
 * @ORM\Table(name="reclusos", uniqueConstraints={@ORM\UniqueConstraint(name="reclusos_pk", columns={"idrecluso"})}, indexes={@ORM\Index(name="personarecluso_fk", columns={"idpersona"}), @ORM\Index(name="ubicacion_fk", columns={"idcarcel"})})
 * @ORM\Entity
 */
class Reclusos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idrecluso", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="reclusos_idrecluso_seq", allocationSize=1, initialValue=1)
     */
    private $idrecluso;

    /**
     * @var integer
     *
     * @ORM\Column(name="numerorecluso", type="integer", nullable=false)
     */
    private $numerorecluso;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="iniciocondena", type="date", nullable=false)
     */
    private $iniciocondena;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fincondena", type="date", nullable=true)
     */
    private $fincondena;

    /**
     * @var string
     *
     * @ORM\Column(name="delito", type="string", length=40, nullable=false)
     */
    private $delito;

    /**
     * @var string
     *
     * @ORM\Column(name="mara", type="string", length=20, nullable=true)
     */
    private $mara;

    /**
     * @var \Personas
     *
     * @ORM\ManyToOne(targetEntity="\IGF\CoreBundle\Entity\Personas", cascade={"persist", "refresh"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpersona", referencedColumnName="idpersona")
     * })
     */
    private $idpersona;

    /**
     * @var \Carcels
     *
     * @ORM\ManyToOne(targetEntity="\IGF\ModeloBundle\Entity\Carcels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcarcel", referencedColumnName="idcarcel")
     * })
     */
    private $idcarcel;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Traslados", mappedBy="idrecluso")
     */
    private $idtraslado;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Programascarcel", mappedBy="idrecluso")
     */
    private $idprogramascarcel;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idtraslado = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idprogramascarcel = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idrecluso
     *
     * @return integer
     */
    public function getIdrecluso()
    {
        return $this->idrecluso;
    }

    /**
     * Set numerorecluso
     *
     * @param integer $numerorecluso
     *
     * @return Reclusos
     */
    public function setNumerorecluso($numerorecluso)
    {
        $this->numerorecluso = $numerorecluso;

        return $this;
    }

    /**
     * Get numerorecluso
     *
     * @return integer
     */
    public function getNumerorecluso()
    {
        return $this->numerorecluso;
    }

    /**
     * Set iniciocondena
     *
     * @param \DateTime $iniciocondena
     *
     * @return Reclusos
     */
    public function setIniciocondena($iniciocondena)
    {
        $this->iniciocondena = $iniciocondena;

        return $this;
    }

    /**
     * Get iniciocondena
     *
     * @return \DateTime
     */
    public function getIniciocondena()
    {
        return $this->iniciocondena;
    }

    /**
     * Set fincondena
     *
     * @param \DateTime $fincondena
     *
     * @return Reclusos
     */
    public function setFincondena($fincondena)
    {
        $this->fincondena = $fincondena;

        return $this;
    }

    /**
     * Get fincondena
     *
     * @return \DateTime
     */
    public function getFincondena()
    {
        return $this->fincondena;
    }

    /**
     * Set delito
     *
     * @param string $delito
     *
     * @return Reclusos
     */
    public function setDelito($delito)
    {
        $this->delito = $delito;

        return $this;
    }

    /**
     * Get delito
     *
     * @return string
     */
    public function getDelito()
    {
        return $this->delito;
    }

    /**
     * Set mara
     *
     * @param string $mara
     *
     * @return Reclusos
     */
    public function setMara($mara)
    {
        $this->mara = $mara;

        return $this;
    }

    /**
     * Get mara
     *
     * @return string
     */
    public function getMara()
    {
        return $this->mara;
    }

    /**
     * Set idpersona
     *
     * @param \IGF\CoreBundle\Entity\Personas $idpersona
     *
     * @return Reclusos
     */
    public function setIdpersona(\IGF\CoreBundle\Entity\Personas $idpersona = null)
    {
        $this->idpersona = $idpersona;

        return $this;
    }

    /**
     * Get idpersona
     *
     * @return \IGF\CoreBundle\Entity\Personas
     */
    public function getIdpersona()
    {
        return $this->idpersona;
    }

    /**
     * Set idcarcel
     *
     * @param \IGF\ModeloBundle\Entity\Carcels $idcarcel
     *
     * @return Reclusos
     */
    public function setIdcarcel(\IGF\ModeloBundle\Entity\Carcels $idcarcel = null)
    {
        $this->idcarcel = $idcarcel;

        return $this;
    }

    /**
     * Get idcarcel
     *
     * @return \IGF\ModeloBundle\Entity\Carcels
     */
    public function getIdcarcel()
    {
        return $this->idcarcel;
    }

    /**
     * Add idtraslado
     *
     * @param \IGF\ModeloBundle\Entity\Traslados $idtraslado
     *
     * @return Reclusos
     */
    public function addIdtraslado(\IGF\ModeloBundle\Entity\Traslados $idtraslado)
    {
        $this->idtraslado[] = $idtraslado;

        return $this;
    }

    /**
     * Remove idtraslado
     *
     * @param \IGF\ModeloBundle\Entity\Traslados $idtraslado
     */
    public function removeIdtraslado(\IGF\ModeloBundle\Entity\Traslados $idtraslado)
    {
        $this->idtraslado->removeElement($idtraslado);
    }

    /**
     * Get idtraslado
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdtraslado()
    {
        return $this->idtraslado;
    }

    /**
     * Add idprogramascarcel
     *
     * @param \IGF\ModeloBundle\Entity\Programascarcel $idprogramascarcel
     *
     * @return Reclusos
     */
    public function addIdprogramascarcel(\IGF\ModeloBundle\Entity\Programascarcel $idprogramascarcel)
    {
        $this->idprogramascarcel[] = $idprogramascarcel;

        return $this;
    }

    /**
     * Remove idprogramascarcel
     *
     * @param \IGF\ModeloBundle\Entity\Programascarcel $idprogramascarcel
     */
    public function removeIdprogramascarcel(\IGF\ModeloBundle\Entity\Programascarcel $idprogramascarcel)
    {
        $this->idprogramascarcel->removeElement($idprogramascarcel);
    }

    /**
     * Get idprogramascarcel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdprogramascarcel()
    {
        return $this->idprogramascarcel;
    }
}
