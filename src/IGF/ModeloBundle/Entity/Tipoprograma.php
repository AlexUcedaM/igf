<?php

namespace IGF\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipoprograma
 *
 * @ORM\Table(name="tipoprograma", uniqueConstraints={@ORM\UniqueConstraint(name="tipoprograma_pk", columns={"idtipoprograma"})})
 * @ORM\Entity
 */
class Tipoprograma
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idtipoprograma", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="tipoprograma_idtipoprograma_seq", allocationSize=1, initialValue=1)
     */
    private $idtipoprograma;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=40, nullable=true)
     */
    private $nombre;


}

