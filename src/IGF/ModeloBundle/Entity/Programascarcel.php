<?php

namespace IGF\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Programascarcel
 *
 * @ORM\Table(name="programascarcel", uniqueConstraints={@ORM\UniqueConstraint(name="programascarcel_pk", columns={"idprogramascarcel"})}, indexes={@ORM\Index(name="programacarcel_fk", columns={"idcarcel"}), @ORM\Index(name="relationship_25_fk", columns={"idusuario"}), @ORM\Index(name="carcelprograma_fk", columns={"idprograma"})})
 * @ORM\Entity
 */
class Programascarcel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idprogramascarcel", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="programascarcel_idprogramascarcel_seq", allocationSize=1, initialValue=1)
     */
    private $idprogramascarcel;

    /**
     * @var string
     *
     * @ORM\Column(name="costo", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $costo;

    /**
     * @var integer
     *
     * @ORM\Column(name="cupo", type="integer", nullable=false)
     */
    private $cupo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechainicio", type="date", nullable=false)
     */
    private $fechainicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechafin", type="date", nullable=false)
     */
    private $fechafin;

    /**
     * @var \Programas
     *
     * @ORM\ManyToOne(targetEntity="Programas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idprograma", referencedColumnName="idprograma")
     * })
     */
    private $idprograma;

    /**
     * @var \Carcels
     *
     * @ORM\ManyToOne(targetEntity="\IGF\ModeloBundle\Entity\Carcels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcarcel", referencedColumnName="idcarcel")
     * })
     */
    private $idcarcel;

    /**
     * @var \SegUsuario
     *
     * @ORM\ManyToOne(targetEntity="\IGF\CoreBundle\Entity\SegUsuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuario", referencedColumnName="id")
     * })
     */
    private $idusuario;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Reclusos", inversedBy="idprogramascarcel")
     * @ORM\JoinTable(name="programarecluso",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idprogramascarcel", referencedColumnName="idprogramascarcel")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idrecluso", referencedColumnName="idrecluso")
     *   }
     * )
     */
    private $idrecluso;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idrecluso = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
