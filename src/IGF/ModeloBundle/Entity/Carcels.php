<?php

namespace IGF\ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carcels
 *
 * @ORM\Table(name="carcels", uniqueConstraints={@ORM\UniqueConstraint(name="carcels_pk", columns={"idcarcel"})}, indexes={ @ORM\Index(name="relationship_28_fk", columns={"iddetalledireccion"}), @ORM\Index(name="administradorcarcel_fk", columns={"idusuarioadministrador"})})
 * @ORM\Entity
 */
class Carcels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idcarcel", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="carcels_idcarcel_seq", allocationSize=1, initialValue=1)
     */
    private $idcarcel;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=40, nullable=true)
     */
    private $nombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="capacidad", type="integer", nullable=true)
     */
    private $capacidad;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidadceldas", type="integer", nullable=true)
     */
    private $cantidadceldas;

    /**
     * @var \SegUsuario
     *
     * @ORM\ManyToOne(targetEntity="\IGF\CoreBundle\Entity\SegUsuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idusuarioadministrador", referencedColumnName="id")
     * })
     */
    private $idusuarioadministrador;


    /**
     * @var \Detalledireccion
     *
     * @ORM\ManyToOne(targetEntity="\IGF\CoreBundle\Entity\Detalledireccion", cascade={"persist", "refresh"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddetalledireccion", referencedColumnName="iddetalledireccion")
     * })
     */
    private $iddetalledireccion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="\IGF\CoreBundle\Entity\SegUsuario", inversedBy="idcarcel")
     * @ORM\JoinTable(name="accesocustodios",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idcarcel", referencedColumnName="idcarcel")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idusuario", referencedColumnName="id")
     *   }
     * )
     */
    private $idusuario;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idusuario = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idcarcel
     *
     * @return integer
     */
    public function getIdcarcel()
    {
        return $this->idcarcel;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Carcels
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set capacidad
     *
     * @param integer $capacidad
     *
     * @return Carcels
     */
    public function setCapacidad($capacidad)
    {
        $this->capacidad = $capacidad;

        return $this;
    }

    /**
     * Get capacidad
     *
     * @return integer
     */
    public function getCapacidad()
    {
        return $this->capacidad;
    }

    /**
     * Set cantidadceldas
     *
     * @param integer $cantidadceldas
     *
     * @return Carcels
     */
    public function setCantidadceldas($cantidadceldas)
    {
        $this->cantidadceldas = $cantidadceldas;

        return $this;
    }

    /**
     * Get cantidadceldas
     *
     * @return integer
     */
    public function getCantidadceldas()
    {
        return $this->cantidadceldas;
    }

    /**
     * Set idusuarioadministrador
     *
     * @param \IGF\CoreBundle\Entity\SegUsuario $idusuarioadministrador
     *
     * @return Carcels
     */
    public function setIdusuarioadministrador(\IGF\CoreBundle\Entity\SegUsuario $idusuarioadministrador = null)
    {
        $this->idusuarioadministrador = $idusuarioadministrador;

        return $this;
    }

    /**
     * Get idusuarioadministrador
     *
     * @return \IGF\CoreBundle\Entity\SegUsuario
     */
    public function getIdusuarioadministrador()
    {
        return $this->idusuarioadministrador;
    }

    /**
     * Set iddetalledireccion
     *
     * @param \IGF\CoreBundle\Entity\Detalledireccion $iddetalledireccion
     *
     * @return Carcels
     */
    public function setIddetalledireccion(\IGF\CoreBundle\Entity\Detalledireccion $iddetalledireccion = null)
    {
        $this->iddetalledireccion = $iddetalledireccion;

        return $this;
    }

    /**
     * Get iddetalledireccion
     *
     * @return \IGF\CoreBundle\Entity\Detalledireccion
     */
    public function getIddetalledireccion()
    {
        return $this->iddetalledireccion;
    }

    /**
     * Add idusuario
     *
     * @param \IGF\CoreBundle\Entity\SegUsuario $idusuario
     *
     * @return Carcels
     */
    public function addIdusuario(\IGF\CoreBundle\Entity\SegUsuario $idusuario)
    {
        $this->idusuario[] = $idusuario;

        return $this;
    }

    /**
     * Remove idusuario
     *
     * @param \IGF\CoreBundle\Entity\SegUsuario $idusuario
     */
    public function removeIdusuario(\IGF\CoreBundle\Entity\SegUsuario $idusuario)
    {
        $this->idusuario->removeElement($idusuario);
    }

    /**
     * Get idusuario
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdusuario()
    {
        return $this->idusuario;
    }
}
