<?php

namespace IGF\SeguridadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IGFSeguridadBundle:Default:index.html.twig');
    }
}
